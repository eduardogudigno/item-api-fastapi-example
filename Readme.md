# Item API example using FastAPI for CRUD operations and Oauth2 security

![Project Image](FastAPI-logo.png)

> FastAPI API example using Oauth2

---

### Table of Contents

- [Description](#description)
- [How to use](#how-to-use)
- [References](#references)
- [Author Info](#author-info)

---
## Description

This API has been build as an example of how to use FastAPI, including database interaction doing CRUD operation and implementation of Oauth2 security applied to use some endpoints. Using some points learned in the Platzi FastAPI course.

### Technologies

- Pydantic
- FastAPI
- SqlAlchemy

---

## How to use

### Installation
1. Clone the repository
```bash
    git clone https://gitlab.com/practice-projects84605/item-api-fastapi-example.git
```

2. Create an environment to install dependencies needed to run the project, for that you can use **venv** module.
```bash
    python3 -m venv venv
```

3. Use the **requirements.txt** file to install packages needed to use the project. Inside the environment you can install all packages using pip. This command install all the packages listed in **requirements.txt**.
```bash
    pip install -r requirements.txt
```

4. Run the project. Get inside project folder and execute the following command.
```bash
    uvicorn main:app --reload --port 8000
```
Your app is running on port 8000 

5. Visit the url http://localhost:8000/. You can see the app is running.

    ![Alt text](APIResponse.png)

    Now you are able to see endpoints available in this url: http://127.0.0.1:8000/docs
    ![Alt text](ItemAPI.png)

### Features

1. **Authentication using Oauth2 and JWT**. For some endpoints to work you need to create an user and configure the credentials to be able to use it

    If you want to see how this work you can use the endpoint ```POST /user/``` and fill in the info:

    ```python
        {
            "name": "string",
            "email": "string",
            "password": "string"
        }
    ```

    After that you now have an user that you can use for logging in and be able to execute operations in the endpoints where credentials are required (/user, /user/login, /item/auth)

    Note: Your token will expire after five minutes

2. **CRUD Operations**. In this case you can use some of the endpoints in the list and execute a specific operation over the DB.

    1. ```POST /item/``` - Creates a new record in DB
    2. ```GET /item/```  - Read all records in DB
    3. ```PUT /item/```  - Update an item based on a given id
    4. ```DELETE /item/``` - Delete an item from DB with a specific id
    5. ```GET /item/{id}``` - Read an item in DB with a specific id
    6. ```GET /item/search/{brand}``` - Read all items in DB with a specific brand name

    Notice that endpoint ```POST /item/auth``` requires authentication, in case you want to use check the step 1 to do it, but don't worry this endpoint works the same way as ```POST /item/```.

3. **Useful operations**. You can check some files that you might find useful:

    - **service_functions.py** located in **services** shows how the Database operations has been implemented to be used as functions whenever is needed inside the project.

    - **auth.py** located in **services** initial part of the logic and functions for authentication and hashing passwords are located there

4. **Testing**. In this case you can run the tests using ```pytest``` this will run all test cases that have been built to check the endpoints work as expected

---

## References

- FastAPI Platzi's course: [FastAPI course](https://platzi.com/cursos/fastapi-modularizacion-datos/)
- FastAPI docs: [FastAPI documentation Oauth2](https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt/)
- Arjan Codes channel: [Arjan codes](https://www.youtube.com/watch?v=9gC3Ot0LoUQ)

---

## Author info

- LinkedIn - [Eduardo Gudiño](https://www.linkedin.com/in/eduardo-gudiño-26451a114)