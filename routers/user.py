"""
Endpoints related to User
"""

from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import APIRouter, HTTPException, status, Depends

from sqlalchemy.orm import Session

from models.user import User as UserModel
from services import dependencies, service, auth
from schemas.user import UserInDB, UserPasswd
from schemas.token import Token


user_router = APIRouter(
    tags=["user"],
    prefix="/user",
)


@user_router.get("/")
def get_user(current_user: UserInDB = Depends(auth.get_current_user)):
    """Get info from user. You need to be authorized to use this endpoint"""

    return {"email": current_user.email, "name": current_user.name}


@user_router.post("/login")
def login(
    form_data: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(dependencies.get_db),
):
    """Create a token to validate user"""

    # Check if user is registered
    email_in_db = service.Service(db=db).get_item_by_tag(
        word=form_data.username, model=UserModel, column_name="email"
    )

    if not email_in_db:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"User with email '{form_data.username}' is not registered",
        )

    # Check if user credentials
    user = auth.authenticate_user(
        db=db,
        user_identifier=form_data.username,
        password=form_data.password,
        get_user_fn=auth.get_user,
    )

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    to_encode = {"sub": form_data.username}

    access_token = auth.create_access_token(to_encode)

    return Token(access_token=access_token, token_type="bearer")


@user_router.post("/", status_code=status.HTTP_201_CREATED, response_model=dict)
def create_user(user: UserPasswd, db: Session = Depends(dependencies.get_db)):
    """Create a new user to log in and use restricted endpoints"""

    # Check if email is already registred
    email_in_db = service.Service(db=db).get_item_by_tag(
        word=user.email, model=UserModel, column_name="email"
    )

    if email_in_db:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"User email '{user.email}' is already registered",
        )

    # Create model to insert user into DB. Creates hash to save into DB
    db_user = UserInDB(
        name=user.name,
        email=user.email,
        hashed_password=auth.get_password_hash(user.password),
    )

    # Insert user into DB
    service.Service(db=db).created_record(model=UserModel, schema=db_user)

    return JSONResponse(
        status_code=status.HTTP_201_CREATED,
        content={"message": "User created successfully"},
    )
