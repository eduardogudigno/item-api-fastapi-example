"""
Main file for ToDoList API
"""

import datetime

from contextlib import asynccontextmanager

from fastapi import status
from fastapi import FastAPI
from fastapi.responses import JSONResponse


from config.database import engine, Base
from routers.item import item_router
from routers.user import user_router

VERSION = "0.0.1"


@asynccontextmanager
async def lifespan(app: FastAPI):
    # Create database model
    Base.metadata.create_all(bind=engine)
    yield


app = FastAPI(lifespan=lifespan)
app.title = "Item API"
app.version = VERSION

app.include_router(router=user_router)
app.include_router(router=item_router)


@app.get("/", status_code=status.HTTP_200_OK)
def home():
    """
    Show a generic message to inform the app is up
    """

    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content={
            "message": "App is running",
            "date": str(datetime.datetime.now()),
            "version": VERSION,
        },
    )
