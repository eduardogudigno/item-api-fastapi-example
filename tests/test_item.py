"""
Api's Test
"""

from fastapi import status
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool

from main import app

from config.database import Base
from services.dependencies import get_db

from . import constants_test

SQLALCHEMY_DATABASE_URL = "sqlite:///:memory:"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    connect_args={"check_same_thread": False},
    poolclass=StaticPool,
)

TestingSessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine
)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def setup() -> None:
    # Create the tables in the test database
    Base.metadata.create_all(bind=engine)


def teardown() -> None:
    # Drop the tables in the test database
    Base.metadata.drop_all(bind=engine)


def test_create_item():
    response = client.post(
        "/item/",
        json=constants_test.ITEM_TO_INSERT_1,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    data = response.json()
    assert data["message"] == "Record created successfully"


def test_get_all_items():
    response = client.post(
        "/item/",
        json=constants_test.ITEM_TO_INSERT_2,
    )

    response = client.get("/item/")

    data = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert [constants_test.ITEM_1, constants_test.ITEM_2] == data


def test_get_item():
    response = client.get(
        "/item/1",
    )

    data = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert constants_test.ITEM_1 == data


def test_update_item():
    response = client.put(
        "/item/?item_id=1",
        json={
            "name": "Computer",
            "description": "A computer",
            "price": 350,
            "brand": "MyBrand",
            "stock_quantity": 5,
            "supplier": "string",
        },
    )

    data = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert data["message"] == "Record updated"

    response = client.get(
        "/item/1",
    )

    data_updated = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert data_updated == {
        "name": "Computer",
        "description": "A computer",
        "price": 350,
        "brand": "MyBrand",
        "id": 1,
        "stock_quantity": 5,
        "supplier": "string",
    }


def test_search_item_by_tag():
    response = client.get(
        "/item/search/MyBrand",
    )

    data = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert data == [
        {
            "name": "Computer",
            "description": "A computer",
            "price": 350,
            "brand": "MyBrand",
            "id": 1,
            "stock_quantity": 5,
            "supplier": "string",
        }
    ]


def test_delete_item():
    response = client.delete("/item/?item_id=1")

    data = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert data["message"] == "Record eliminated"


def test_wrong_data_to_insert():
    response = client.post(
        "/item/",
        json=constants_test.WRONG_ITEM_TO_INSERT,
    )

    data = response.json()

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert "String should have at least" in data["detail"][0]["msg"]
