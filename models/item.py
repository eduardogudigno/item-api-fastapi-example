"""
Item's sqlalchemy model definition
"""

from sqlalchemy import NVARCHAR, Column, Integer, String, Float

from config.database import Base


class Item(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    price = Column(Float)
    brand = Column(NVARCHAR(20))
    stock_quantity = Column(Integer)
    supplier = Column(NVARCHAR(50))
