"""
Item's pydantic model definition
"""

from pydantic import BaseModel, Field


class Item(BaseModel):
    """Item's model"""

    name: str = Field(..., min_length=3, max_length=50)
    description: str | None = None
    price: float
    brand: str = Field(default=None, max_length=20)
    stock_quantity: int
    supplier: str = Field(default=None, max_length=50)


class UpdateItem(BaseModel):
    """Item's model

    This model accepts all parameters as optional for update

    """

    name: str = Field(default=None, min_length=3, max_length=50)
    description: str | None = None
    price: float | None
    brand: str = Field(default=None, max_length=20)
    stock_quantity: int | None = None
    supplier: str = Field(default=None, max_length=50)


class ItemPrimaryKey(Item):
    """Foreign key for item model"""

    id: int
