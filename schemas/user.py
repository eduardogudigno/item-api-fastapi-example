"""
User pydantic model definition
"""

from pydantic import BaseModel, Field


class User(BaseModel):
    """Item's model"""

    name: str = Field(..., min_length=3, max_length=50)
    email: str


class UserInDB(User):
    """Hashed password for user"""

    hashed_password: str


class UserPasswd(User):
    """User password"""

    password: str


class UserLogin(BaseModel):
    email: str
    password: str
