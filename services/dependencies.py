"""
Dependencies needed for the applicantion to work
"""

from sqlalchemy.orm import Session
from fastapi import HTTPException, status

from config.database import SessionLocal, Base
from services.service import Service


def get_db():
    """Handling database connection"""

    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_valid_record_by_id(
    db: Session, id_record: int, model: Base, id_column: str = "id"
) -> Base | None:
    """Get a record from db using a specified id
    Args:
        db: Database session
        id_record: id to find
        model: Database sqlalchemy mode to use
        id_column: column name to get the record

    Returns
        Base | None | HTTPException
    """

    record = Service(db=db).get_item_by_id(
        id_record=id_record, model=model, id_column=id_column
    )

    # if record does not exist raise an http exception
    if not record:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Record with id '{id_record}' does not exists",
        )

    return record
