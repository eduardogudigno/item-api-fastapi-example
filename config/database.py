"""
Database connection configuration
"""

import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.orm import declarative_base

SQLITE_DB_NAME = "../database.sqlite"

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
DATABASE_URL = f"sqlite:///{os.path.join(BASE_DIR,SQLITE_DB_NAME)}"

# check_same_thread argument is only needed for sqlite connections
engine = create_engine(
    DATABASE_URL, connect_args={"check_same_thread": False}, echo=True
)

SessionLocal = sessionmaker(bind=engine)

Base = declarative_base()
